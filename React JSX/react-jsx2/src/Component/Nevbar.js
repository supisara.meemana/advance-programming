import React from 'react';
import logo from '../Component/pikkanode.png';
import './Nevbar.css'


function Nevbar() {
    return (
        <div>
            <ul>
            <li> <img src={logo} alt='Logo' width={70} height={35} /> </li>
                <li><a href="#home">Home</a></li>
                <li><a href="#news">News</a></li>
                <li><a href="#contact">Contact</a></li>
                <li><a href="#about">About</a></li>
                
            </ul>
        </div>
        
    );
}
export default Nevbar