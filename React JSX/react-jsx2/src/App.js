
import './App.css';
import Nevbar from './Component/Nevbar';
import PictureCard from './Component/PictureCard';
import Footer from './Component/Footer';
const row = [0, 1, 2, 3, 4, 5, 6, 7]
function App() {
  return (
    <div>
      <Nevbar />
      <div className="row">
        {row.map(c => (
          <div className="col-md-3">
            <PictureCard />
          </div>
        ))
        }
      </div>
      <Footer/>
    </div>
  );

}
export default App;
