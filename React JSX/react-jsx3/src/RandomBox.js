const RandomNumberColor = () => Math.floor(Math.random() *5)
const colors = ['red', 'blue', 'green', 'purple', 'pink'];
const randomFontSize = () => Math.floor(Math.random()*20+20);
const color = colors[RandomNumberColor()];
const fontSize = randomFontSize();
function RandomBox() {
    
    return (
      
            <div style={{ width: "400px", height: "200px", backgroundColor: color, color: 'white' , textAlign :"center" ,paddingTop:"150px" }}>
                <div>
                <p style={{fontSize:fontSize}}>Random Box</p>
                </div>
            </div>
       
    )


}
export default RandomBox;