function Box({ background }) {
    return (<div className='box'
        style={{
            background,
            border: "1px solid black",
        }}>
    </div>
    )
}
export default Box;