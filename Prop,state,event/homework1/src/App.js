
import './App.css';
import Nevbar from './Component/Nevbar';
import PictureCard from './Component/PictureCard';
import Footer from './Component/Footer';

const row = [0, 1, 2, 3, 4, 5, 6, 7]
const pictureData = {id: 1,
  imgSrc: 'https://unsplash.it/150/200?image=1',
  createBy: "Kluoychab",
  date:"24/12/2021",
  likeCount: 100,
  commentCount: 1000,
 }
function App() {
  return (
    <div>
      <Nevbar />
      <div className="row">
        {row.map((c,i) => (
          <div className="col-md-3">
            <PictureCard pictureData={pictureData} key={i}/>
          </div>
        ))
        }
      </div>
      <Footer/>
    </div>
  );

}
export default App;
