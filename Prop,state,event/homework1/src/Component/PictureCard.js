import React from "react";
import './Picturecard.css';
function PictureCard(props) {
  console.log(props);
    return (
        <div className="card">
        <img src={props.pictureData.imgSrc} alt="Avatar" width = {200} height = {150}/>
        <div className="container">
          <h4><b>Date:{props.pictureData.date}</b></h4>
          <p>Like:{props.pictureData.likeCount}</p>
          <p>Comment:{props.pictureData.commentCount}</p>
          <p>createBy:{props.pictureData.createBy}</p>
        </div>
      </div>
    );
}
export default PictureCard