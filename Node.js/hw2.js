let alphabet = ["A","B","C","D","E","F","G","H","I","J","K","L",
"M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];
function odd(i) {
    return alphabet[i];
}
function even(j) {
    return alphabet[j];
}
async function callalphabet() {
    let i = 0;
    let j = 1;
    let C = ""
    for(let k=1;k<=13;k++){
        let A = await even(j);
        let B = await odd(i);
        C+=A+" "+B+" "
        i+=2
        j+=2
    }
    console.log(C)
}
try {
    callalphabet();
 } catch (error) {
    console.log("Error name: " + error.name);
    console.log("Error message: " + error.message);
 }

 